import React, { useState } from 'react';
import './App.css';
import { Button, Card, CardActionArea, CardContent, CardMedia, Grid, Stack, Typography } from '@mui/material';

type Pokemon = {
    name: string;
    url: string;
};

const API_URL = 'https://pokeapi.co/api/v2/pokemon?limit=10&offset=';
const IMAGE_URL = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';

function App() {
    const [apiData, setApiData] = useState(Array<Pokemon>);
    const [counter, setCounter] = useState(0);

    const loadMore = async (): Promise<void> => {
        fetch(`${API_URL}${counter}`)
            .then((response) => response.json())
            .then((json) => setApiData(apiData.concat(json.results)))
            .catch((error) => console.error(error));
        setCounter(counter + 10);
    };

    const removePokemon = (pokemon: Pokemon): void => {
        setApiData(apiData.filter((item: Pokemon): boolean => item !== pokemon));
    };

    const getImageUrl = (url: string): string => {
        const splitUrl: string[] = url.split('/');

        return `${IMAGE_URL}${splitUrl[splitUrl.length - 2]}.png`;
    };

    return (
        <div className="App">
            <Stack spacing={2}>
                <Grid
                    container
                    rowSpacing={{ xs: 1, sm: 2, md: 3 }}
                    columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                    alignItems="center"
                    justifyContent="center"
                >
                    {apiData.map((pokemon: Pokemon, i: number) => {
                        return (
                            <Grid item xs={2} sx={{ minWidth: 250 }} key={i} onClick={() => removePokemon(pokemon)}>
                                <Card sx={{ maxWidth: 345 }}>
                                    <CardActionArea>
                                        <CardMedia
                                            className={'mediaCard'}
                                            component="img"
                                            height="140"
                                            image={getImageUrl(pokemon?.url)}
                                            alt={pokemon?.name}
                                            sx={{ objectFit: 'contain' }}
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="div">
                                                {pokemon?.name?.toUpperCase()}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>
                        );
                    })}
                </Grid>
                <Button onClick={loadMore}>{counter === 0 ? 'Load Pokemons' : 'Load more'}</Button>
            </Stack>
        </div>
    );
}

export default App;
